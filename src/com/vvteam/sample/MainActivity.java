package com.vvteam.sample;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.vvteam.amazingbars.*;
import com.vvteam.amazingbars.data.Bar;
import com.vvteam.amazingbars.properties.BarsProperty;

import java.util.Random;

public class MainActivity extends Activity {

    public static final Random rnd = new Random(System.currentTimeMillis());

    private BarsView barsView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        barsView = (BarsView) findViewById(R.id.bars);
        for (int i = 0; i < 40; i++) {
            addRndBar();
        }
        barsView.setShowAllValues(false);

        Button bAdd = (Button) findViewById(R.id.bAdd);
        bAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addRndBar();
            }
        });
        Button bRemove = (Button) findViewById(R.id.bRemove);
        bRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (barsView.size() > 0)
                    barsView.removeBar(rnd.nextInt(barsView.size()));
            }
        });

        Button bClear = (Button) findViewById(R.id.bClear);
        bClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barsView.removeAll();
            }
        });

        LinearLayout llProperties = (LinearLayout) findViewById(R.id.llProperties);
        matchWidth.setMargins(0, 15, 0, 15);
        matchHeight.setMargins(0, 15, 0, 15);
        wrapAll.setMargins(0, 15, 0, 15);


        for (BarsProperty p : barsView.getProperties()){
            llProperties.addView(createSeekPropertyLayout(p));
        }



        llProperties.addView(createCheckPropertyLayout("Show all values: ",  false,
                new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            barsView.setShowAllValues(b);
                        }
        }));

        llProperties.addView(createCheckPropertyLayout("Remember selection: ",  false,
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        barsView.setRememberSelection(b);
                    }
                }));

        llProperties.addView(createRadioGroupPropertyLayout("Alignment: ", Alignment.values(), Alignment.CENTER.ordinal(), new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                barsView.setBarsAlignment(Alignment.values()[i]);
            }
        }));

        llProperties.addView(createRadioGroupPropertyLayout("Orientation: ", Orientation.values(), Orientation.VERTICAL.ordinal(), new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                barsView.setBarsOrientation(Orientation.values()[i]);
            }
        }));

        llProperties.addView(createRadioGroupPropertyLayout("Animation Style: ", new Enum[] {AnimationStyle.NARROW, AnimationStyle.FIT}, AnimationStyle.FIT.ordinal(), new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                barsView.setBarsAnimationStyle(AnimationStyle.values()[i]);
            }
        }));
    }



    private LinearLayout.LayoutParams matchWidth = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    private LinearLayout.LayoutParams matchHeight = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
    private LinearLayout.LayoutParams wrapAll = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    private LinearLayout createSeekPropertyLayout(final BarsProperty property){
        LinearLayout propertyLayout = new LinearLayout(this);
        propertyLayout.setOrientation(LinearLayout.HORIZONTAL);
        propertyLayout.setLayoutParams(matchWidth);

        final TextView textView = new TextView(this);
        textView.setLayoutParams(matchHeight);
        textView.setText(String.format(property.getName() + ": %d", (int) property.getValue()));

        SeekBar seekBar = new SeekBar(this);
        seekBar.setLayoutParams(matchWidth);
        seekBar.setMax((int) property.getRange());
        seekBar.setProgress((int) (property.getValue() + property.getMin()));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
                if (fromUser) {
                    int val = (int) (i + property.getMin());
                    textView.setText(String.format(property.getName() + ": %d", val));
                    property.setValue(val);
                    barsView.update();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        propertyLayout.addView(textView);
        propertyLayout.addView(seekBar);
        return propertyLayout;
    }
    private LinearLayout createCheckPropertyLayout(String title, boolean defaultValue, CompoundButton.OnCheckedChangeListener listener){
        LinearLayout propertyLayout = new LinearLayout(this);
        propertyLayout.setOrientation(LinearLayout.HORIZONTAL);
        propertyLayout.setLayoutParams(matchWidth);
        final TextView textView = new TextView(this);
        textView.setLayoutParams(matchHeight);
        textView.setText(title);

        CheckBox checkBox = new CheckBox(this);
        checkBox.setLayoutParams(wrapAll);
        checkBox.setChecked(defaultValue);
        checkBox.setOnCheckedChangeListener(listener);

        propertyLayout.addView(textView);
        propertyLayout.addView(checkBox);
        return propertyLayout;
    }
    private LinearLayout createRadioGroupPropertyLayout(String title, Enum[] labels, int selectedRadioIndex, RadioGroup.OnCheckedChangeListener listener){
        LinearLayout propertyLayout = new LinearLayout(this);
        propertyLayout.setOrientation(LinearLayout.HORIZONTAL);
        propertyLayout.setLayoutParams(matchWidth);
        final TextView textView = new TextView(this);
        textView.setLayoutParams(matchHeight);
        textView.setText(title);

        RadioGroup radioGroup = new RadioGroup(this);
        radioGroup.setLayoutParams(wrapAll);
        radioGroup.setOrientation(LinearLayout.HORIZONTAL);
        radioGroup.setSaveEnabled(false);
        for (int i = 0; i < labels.length; ++i){
            RadioButton radioButton = new RadioButton(this);
            radioButton.setSaveEnabled(false);
            radioButton.setLayoutParams(wrapAll);
            radioButton.setId(i);
            radioGroup.addView(radioButton);
            radioButton.setText(labels[i].toString());
            radioButton.setChecked(i == selectedRadioIndex);
        }

        radioGroup.setOnCheckedChangeListener(listener);

        propertyLayout.addView(textView);
        propertyLayout.addView(radioGroup);
        return propertyLayout;
    }


    private void addRndBar(){
        int val = rnd.nextInt(500) + 20;
        barsView.addBar(new Bar(val));
    }


}
