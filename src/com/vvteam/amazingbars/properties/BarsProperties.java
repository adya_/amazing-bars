package com.vvteam.amazingbars.properties;


public class BarsProperties {


    private BarsProperties(){}

    public static final String BAR_SPACING = "Bar Spacing";


    public static final String VIEW_PADDING = "View Padding";
    public static final String VIEW_BORDER = "View Border";

    public static final String BAR_LABEL_SIZE = "Bar Label Size";
    public static final String BAR_LABEL_ALPHA = "Bar Label Alpha";
    public static final String BAR_THICKNESS = "Bar Thickness";
    public static final String BAR_BORDER_THICKNESS = "Bar Border Thickness";
}
