package com.vvteam.amazingbars.properties;

/** Created to be used with API level lower than 14. */
public class NoSuchPropertyException extends RuntimeException {

    public NoSuchPropertyException(String detailMessage) {
        super(detailMessage);
    }
}
