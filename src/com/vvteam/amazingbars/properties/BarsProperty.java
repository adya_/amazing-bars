package com.vvteam.amazingbars.properties;

import java.util.Observable;

/** Struct which contains int value and range of possible values. */
public class BarsProperty extends Observable{

    private String name;

    private float min;
    private float value;
    private float max;

    /** Construct Property with given range.
     * @param min Lower bound for this property.
     * @param max Upper bound for this property.
     */
    public BarsProperty(float min, float max) {
        this(min, max, "");

    }

    /** Construct Property with given range and name.
     * @param min Lower bound for this property.
     * @param max Upper bound for this property.
     * @param name Name of this property.
     */
    public BarsProperty(float min, float max, String name) {
        this(min, max, min, name);
    }

    /** Construct Property with given range, specified initial value, and name.
     * @param min Lower bound for this property.
     * @param max Upper bound for this property.
     * @param initialValue Value which is set upon initialization.
     * @param name Name of this property.
     */
    public BarsProperty(float min, float max, float initialValue, String name) {
        this.min = Math.min(min, max);
        this.max = Math.max(min, max);
        this.name = name;
        setValue(initialValue);
    }

    public BarsProperty(final BarsProperty property) {
        this(property.getMin(), property.getMax(), property.getValue(), property.getName());
    }

    public String getName() {
        return name;
    }
    public float getMin() {
        return min;
    }
    public float getMax() {
        return max;
    }

    public float getValue() {
        return value;
    }
    public void setValue(float value) {
        this.value = clamp(min, value, max);
        setChanged();
        notifyObservers(name);
    }

    public float getRange(){
        return Math.abs(max - min);
    }

    /** Ensures that value is within range. */
    private float clamp(float min, float value, float max){
        return (value < min ? min : (value > max ? max : value));
    }

    @Override
    public String toString() {
        return String.format("%.2f", value);
    }
}
