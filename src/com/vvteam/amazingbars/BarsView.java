package com.vvteam.amazingbars;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import com.vvteam.amazingbars.computators.ViewAspectRatioMeasurer;
import com.vvteam.amazingbars.computators.animated.HorizontalBarsFitAnimatedComputator;
import com.vvteam.amazingbars.computators.animated.HorizontalBarsNarrowAnimatedComputator;
import com.vvteam.amazingbars.computators.animated.VerticalBarsFitAnimatedComputator;
import com.vvteam.amazingbars.computators.animated.VerticalBarsNarrowAnimatedComputator;
import com.vvteam.amazingbars.computators.normal.BarsComputator;
import com.vvteam.amazingbars.data.Bar;
import com.vvteam.amazingbars.data.BarColors;
import com.vvteam.amazingbars.data.BarsCollection;
import com.vvteam.amazingbars.data.BarsData;
import com.vvteam.amazingbars.properties.BarsProperties;
import com.vvteam.amazingbars.properties.BarsProperty;
import com.vvteam.amazingbars.properties.NoSuchPropertyException;
import com.vvteam.amazingbars.renderers.BarsRenderer;
import com.vvteam.amazingbars.renderers.VerticalBarsRenderer;

import java.util.Collection;
import java.util.HashMap;

public class BarsView extends View implements BarsCollection{

    private BarsRenderer barsRenderer;
    private BarsData barsData;
    private BarsTouchHandler barsTouchHandler;
    private BarsComputator barsComputator;
    private BarsComputator customComputator;

    private float aspectRatio = 2;
    private ViewAspectRatioMeasurer measurer = new ViewAspectRatioMeasurer(aspectRatio);

    private Bar selectedBar = null;
    private int selectedIndex = -1;
    private boolean rememberSelection = false;
    private boolean useMainStyle = true;

    private boolean showAllValues = true;

    private int backgroundColor = Color.DKGRAY;
    private int borderColor = Color.LTGRAY;

    protected HashMap<String, BarsProperty> barsProperties;
    protected BarColors defaultBarColors = new BarColors(Color.WHITE, Color.LTGRAY, Color.WHITE, Color.BLACK, Color.GREEN, Color.BLUE, Color.GREEN, Color.BLACK);

    private boolean isOutadated = false; // flag indicates whether bars should be redrawn

    private Alignment barsAlignment = Alignment.CENTER;
    private AnimationStyle barsAnimationStyle = AnimationStyle.FIT;
    private Orientation barsOrientation = Orientation.VERTICAL;

    public void resetProperties() {
        barsProperties.clear();
        barsProperties.put(BarsProperties.BAR_SPACING, new BarsProperty(2, 15, 2, BarsProperties.BAR_SPACING));
        barsProperties.put(BarsProperties.BAR_THICKNESS, new BarsProperty(2, 40, 20, BarsProperties.BAR_THICKNESS));
        barsProperties.put(BarsProperties.VIEW_PADDING, new BarsProperty(2, 30, 5, BarsProperties.VIEW_PADDING));
        barsProperties.put(BarsProperties.VIEW_BORDER, new BarsProperty(0, 10, 6, BarsProperties.VIEW_BORDER));
        barsProperties.put(BarsProperties.BAR_LABEL_SIZE, new BarsProperty(8, 18, 12, BarsProperties.BAR_LABEL_SIZE));
        barsProperties.put(BarsProperties.BAR_BORDER_THICKNESS, new BarsProperty(0,5,2, BarsProperties.BAR_BORDER_THICKNESS));
        barsProperties.put(BarsProperties.BAR_LABEL_ALPHA, new BarsProperty(0, 1, 0.6f, BarsProperties.BAR_LABEL_ALPHA));
        update();
    }

    /** Enumerate all properties of the bars. */
    public Collection<BarsProperty> getProperties(){
        return barsProperties.values();
    }

    public BarsProperty getProperty(String name){
        BarsProperty prop = barsProperties.get(name);
        if (prop == null){
            throw new NoSuchPropertyException("Property '" + name + "' doesn't exist.");
        }
        return prop;
    }

    public void setProperty(BarsProperty property){
        barsProperties.put(property.getName(), new BarsProperty(property));
        update();
    }

    private void init() {
        setSaveEnabled(true);
        barsData = new BarsData();
        barsTouchHandler = new BarsTouchHandler(this);
        this.setOnTouchListener(barsTouchHandler);
        customComputator = null;
        barsProperties = new HashMap<>();
        barsRenderer = new VerticalBarsRenderer(this);
        barsComputator = new VerticalBarsNarrowAnimatedComputator(this);
        resetProperties();
    }

    public BarsView(Context context) {
        super(context);
        init();
    }

    public BarsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BarsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public BarsData getBarsData() {
        return barsData;
    }

    public void setBarsData(BarsData barsData) {
        this.barsData = barsData;
        update();
    }

    public void setDefaultColorsPallete(BarColors defaultColorsPallete) {
        this.defaultBarColors.set(defaultColorsPallete);

    }

    public BarsTouchHandler getTouchHandler() {
        return barsTouchHandler;
    }


    public Alignment getBarsAlignment() {
        return barsAlignment;
    }

    public void setBarsAlignment(Alignment barsAlignment) {
        this.barsAlignment = barsAlignment;
        update();
    }

    public Orientation getBarsOrientation() {
        return barsOrientation;
    }

    public void setBarsOrientation(Orientation barsOrientation) {
        this.barsOrientation = barsOrientation;
        setupComputator();
    }

    public AnimationStyle getBarsAnimationStyle() {
        return barsAnimationStyle;
    }

    public void setBarsAnimationStyle(AnimationStyle barsAnimationStyle) {
        this.barsAnimationStyle = barsAnimationStyle;
        setupComputator();
    }

    private void setupComputator() {
        switch (barsOrientation) {
            case VERTICAL:
                switch (barsAnimationStyle) {
                    case NARROW:
                        barsComputator = new VerticalBarsNarrowAnimatedComputator(this);
                        break;
                    case FIT:
                        barsComputator = new VerticalBarsFitAnimatedComputator(this);
                        break;
                    case CUSTOM:
                        if (customComputator == null) {
                            throw new NullPointerException("Cusom Computator was not specified. Please set up custom computator first.");
                        }
                        barsComputator = customComputator;
                        break;
                }
                break;
            case HORIZONTAL:
                switch (barsAnimationStyle) {
                    case NARROW:
                        barsComputator = new HorizontalBarsNarrowAnimatedComputator(this);
                        break;
                    case FIT:
                        barsComputator = new HorizontalBarsFitAnimatedComputator(this);
                        break;
                    case CUSTOM:
                        if (customComputator == null) {
                            throw new NullPointerException("Cusom Computator was not specified. Please set up custom computator first.");
                        }
                        barsComputator = customComputator;
                        break;
                }

                break;
        }
        barsComputator.setScreenSize(measurer.getMeasuredWidth(), measurer.getMeasuredHeight());
        invalidate();
    }


    public int getBackgroundColor() {
        return backgroundColor;
    }

    @Override
    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        invalidate();
    }

    public int getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(int borderColor) {
        this.borderColor = borderColor;
        invalidate();
    }

    public boolean isRememberSelection() {
        return rememberSelection;
    }

    public void setRememberSelection(boolean rememberSelection) {
        this.rememberSelection = rememberSelection;
    }

    public boolean isUseMainStyle() {
        return useMainStyle;
    }

    public void setUseMainStyle(boolean useMainStyle) {
        this.useMainStyle = useMainStyle;
        if (this.useMainStyle){
            for (Bar b : barsData){
                setupBar(b);
            }
            invalidate();
        }
    }

    public boolean isShowAllValues() {
        return showAllValues;
    }

    public void setShowAllValues(boolean showAllValues) {
        this.showAllValues = showAllValues;
        getComputator().recalculateLabels();
        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        measurer.measure(widthMeasureSpec, heightMeasureSpec);
        float width = measurer.getMeasuredWidth();
        float height = measurer.getMeasuredHeight();
        barsComputator.setScreenSize(width, height);
        setMeasuredDimension((int) width, (int) height);
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        if (width > 0 && height > 0) {
            barsComputator.setScreenSize(width, height);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (isOutadated) {
            super.onDraw(canvas);
            barsRenderer.draw(canvas);
            isOutadated = false;
        }
    }

    public BarsComputator getComputator() {
        return barsComputator;
    }

    public BarsComputator getCustomComputator() {
        return customComputator;
    }

    public void setCustomComputator(BarsComputator customComputator) {
        this.customComputator = customComputator;
    }

    public void selectBar(int selectedBarIndex) {
        if (selectedBar != null) {  // didn't call deselectBar() to avoid extra call to invalidate().
            this.selectedBar.setSelected(false);
            this.selectedIndex = -1;
        }
        selectedBar = barsData.getBar(selectedBarIndex);
        if (selectedBar != null) {
            this.selectedBar.setSelected(true);
            this.selectedIndex = selectedBarIndex;
            invalidate();
        }
    }
    public void deselectBar() {
        if (selectedBar != null) {
            this.selectedBar.setSelected(false);
            this.selectedIndex = -1;
            invalidate();
        }
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public Bar getSelectedBar() {
        return selectedBar;
    }

    private void setupBar(Bar bar){
        bar.setColorsPallete(defaultBarColors);
        bar.setProperty(getProperty(BarsProperties.BAR_THICKNESS));
        bar.setProperty(getProperty(BarsProperties.BAR_BORDER_THICKNESS));
        bar.setProperty(getProperty(BarsProperties.BAR_SPACING));
        bar.setProperty(getProperty(BarsProperties.BAR_LABEL_SIZE));
        bar.setProperty(getProperty(BarsProperties.BAR_LABEL_ALPHA));
    }

    public void update() {
        if (useMainStyle) {
            for (Bar b : barsData)
                setupBar(b);
        }
        getComputator().recalculateMetrics();
        invalidate();
    }

    @Override
    public void invalidate(){
        isOutadated = true;
        super.invalidate();
    }


    // Forwarding interface

    @Override
    public boolean addBar(Bar bar) {
        if (barsData.addBar(bar)){
            update();
            return true;
        }
        return false;
    }

    @Override
    public boolean addBar(int index, Bar bar) {
        if (barsData.addBar(index, bar)){
            update();
            return true;
        }
        return false;
    }

    @Override
    public boolean addBars(Collection<Bar> bars) {
        if (barsData.addBars(bars)){
            update();
            return true;
        }
        return false;
    }

    @Override
    public boolean removeBar(Bar bar) {
        if (barsData.removeBar(bar)){
            update();
            return true;
        }
        return false;
    }

    @Override
    public boolean removeBar(int index) {
        if (barsData.removeBar(index)){
            update();
            return true;
        }
        return false;
    }

    @Override
    public void removeAll() {
        barsData.removeAll();
        update();
    }

    @Override
    public boolean contains(Bar bar) {
        return barsData.contains(bar);
    }

    public Bar getBar(int index) {
        return barsData.getBar(index);
    }

    @Override
    public int indexOf(Bar bar) {
        return barsData.indexOf(bar);
    }

    @Override
    public int size() {
        return barsData.size();
    }

}
