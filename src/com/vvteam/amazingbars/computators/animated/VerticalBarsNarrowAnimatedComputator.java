package com.vvteam.amazingbars.computators.animated;


import com.vvteam.amazingbars.BarsView;
import com.vvteam.amazingbars.computators.Side;
import com.vvteam.amazingbars.computators.normal.VerticalBarsComputator;
import com.vvteam.amazingbars.data.Bar;
import com.vvteam.amazingbars.properties.BarsProperties;
import com.vvteam.amazingbars.properties.BarsProperty;

/** Computator which computes bars' positions with shifting and narrowing. */
public class VerticalBarsNarrowAnimatedComputator extends VerticalBarsComputator {

    private float yDisplacement = 50;      // base displacement of selected bar.
    private float animatedRange = 7;       // amount of animated bars from both sides of selected bar.
    private float yStepFactor = 0.5f;      // step multiplier. Lowering away from selected bar.
    private float spacingFactor = 1.2f;    // spacing multiplier. Lowering away from selected bar.
    private float spacingSizeAdj = 0.008f; // adjust spacing multiplier based on how great is base spacing (to avoid overlapping when default spacing was set too big).
    private float thicknessFactor = 0.98f; // thickness multiplier. Lowering away from selected bar.



    public VerticalBarsNarrowAnimatedComputator(BarsView barsView) {
        super(barsView);
    }

    @Override
    protected void recalculateAnimatedBarsSize() {
        final float adaptedThicknessFactor = thicknessFactor - 0.5f / (float)barsView.size();

        barsWidth = barsHeight = 0;

        final int selectedIndex = barsView.getSelectedIndex();
        int animatedStep;

        float x = 0;
        float y;
        float yStep;

        final float defaultSpacing = barsView.getProperty(BarsProperties.BAR_SPACING).getValue();
        final float defaultThickness = barsView.getProperty(BarsProperties.BAR_THICKNESS).getValue();
        float thickness;
        float spacing = defaultSpacing;

        for (int i = 0; i < barsView.size(); ++i) {
            Bar b = barsView.getBar(i);
            BarsProperty barThickness = b.getProperty(BarsProperties.BAR_THICKNESS);
            BarsProperty barSpacing = b.getProperty(BarsProperties.BAR_SPACING);

            animatedStep = Math.abs(selectedIndex - i); // get distance to the selected bar

            if (animatedStep <= animatedRange){ // current bar should be shifted
                spacing = (float) (defaultSpacing * Math.pow(spacingFactor - spacingSizeAdj * defaultSpacing, animatedRange - animatedStep));
                yStep = (float) (yDisplacement * Math.pow(yStepFactor, animatedStep));
            }
            else{
                spacing = defaultSpacing;
                yStep = 0;// no displacement
            }

            if (animatedStep > animatedRange)
                thickness = (float) (defaultThickness * Math.pow(adaptedThicknessFactor, animatedStep - animatedRange));

            else {
                thickness = defaultThickness;
            }

            barThickness.setValue(thickness);
            barSpacing.setValue(spacing);

            x += spacing; // add leading spacing
            float xEnd = x + thickness;
            x = xEnd + spacing;

            y = b.getValue() + yStep;
            if (y > barsHeight) barsHeight = y;
        }
        float firstSpaceing = barsView.getBar(0).getProperty(BarsProperties.BAR_SPACING).getValue();
        float lastSpasing = barsView.getBar(barsView.size() - 1).getProperty(BarsProperties.BAR_SPACING).getValue();
        barsWidth = (x - firstSpaceing - lastSpasing);
        barsHeight /= 0.7;
    }

    @Override
    protected void recalculateAnimatedBarsScale() {
        float drawingWidth = getDrawingWidth(); // width of the drawing area

        widthScaleFactor = drawingWidth / barsWidth;
        widthScaleFactor = (widthScaleFactor > 1 ? 1 : widthScaleFactor);

        // leave height scaling as in normal state
    }

    @Override
    protected void recalculateAnimatedBarsPositions() {

        switch (barsView.getBarsAlignment()) {
            case LEFT:
                startX =  getBarsBorder(Side.LEFT);
                break;
            case RIGHT:
                startX = getBarsBorder(Side.RIGHT) - getBarsWidth() * widthScaleFactor;
                break;
            case CENTER:
                startX = (getScreenWidth() - getBarsWidth() * widthScaleFactor) / 2;
                startX = (startX < getBarsBorder(Side.LEFT) ? getBarsBorder(Side.LEFT) : startX);
                break;
        }
        startY = getBarsBorder(Side.BOTTOM);

        final int selectedIndex = barsView.getSelectedIndex();
        int animatedStep;

        float x = startX;

        float y;
        float yStep;
        float thickness;
        float spacing;

        for (int i = 0; i < barsView.getBarsData().size(); ++i) {
            Bar b = barsView.getBarsData().getBar(i);
            thickness = b.getProperty(BarsProperties.BAR_THICKNESS).getValue() * widthScaleFactor;
            spacing =  b.getProperty(BarsProperties.BAR_SPACING).getValue() * widthScaleFactor;

            animatedStep = Math.abs(selectedIndex - i); // get distance to the selected bar

            if (animatedStep <= animatedRange){ // current bar should be shifted
                yStep = (float) (yDisplacement * Math.pow(yStepFactor, animatedStep));
            }
            else{
                yStep = 0;// no displacement
            }
            x += spacing; // add leading spacing
            y = startY - b.getValue() * heightScaleFactor - yStep;
            float xEnd = x + thickness;
            xEnd = (xEnd >= getBarsBorder(Side.RIGHT) ? getBarsBorder(Side.RIGHT) : xEnd);
            b.getDrawingRect().set(x, y, xEnd, startY - yStep);
            x = xEnd + spacing;
        }
    }

    @Override
    protected void recalculateAnimatedBarsLabels() {
        super.recalculateBarsLabels();
    }
}
