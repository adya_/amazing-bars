package com.vvteam.amazingbars.computators.animated;


import com.vvteam.amazingbars.BarsView;
import com.vvteam.amazingbars.computators.Side;
import com.vvteam.amazingbars.computators.normal.HorizontalBarsComputator;
import com.vvteam.amazingbars.data.Bar;
import com.vvteam.amazingbars.properties.BarsProperties;
import com.vvteam.amazingbars.properties.BarsProperty;

public class HorizontalBarsNarrowAnimatedComputator extends HorizontalBarsComputator{
    public HorizontalBarsNarrowAnimatedComputator(BarsView barsView) {
        super(barsView);
    }

    private float xDisplacement = 50;      // base displacement of selected bar.
    private float animatedRange = 7;       // amount of animated bars from both sides of selected bar.
    private float xStepFactor = 0.5f;      // step multiplier. Lowering away from selected bar.
    private float spacingFactor = 1.2f;    // spacing multiplier. Lowering away from selected bar.
    private float spacingSizeAdj = 0.008f; // adjust spacing multiplier based on how great is base spacing (to avoid overlapping when default spacing was set too big).
    private float thicknessFactor = 0.98f; // thickness multiplier. Lowering away from selected bar.

    @Override
    protected void recalculateAnimatedBarsSize() {
        final float adaptedThicknessFactor = thicknessFactor - 0.5f / (float)barsView.size();

        barsWidth = barsHeight = 0;

        int selectedIndex = barsView.getSelectedIndex();
        int animatedStep;

        float y = 0;
        float x = 0;
        float xStep;

        float defaultSpacing = barsView.getProperty(BarsProperties.BAR_SPACING).getValue();
        float defaultThickness = barsView.getProperty(BarsProperties.BAR_THICKNESS).getValue();
        float thickness;
        float spacing = defaultSpacing;

        for (int i = 0; i < barsView.size(); ++i) {
            Bar b = barsView.getBar(i);
            BarsProperty barThickness = b.getProperty(BarsProperties.BAR_THICKNESS);
            BarsProperty barSpacing = b.getProperty(BarsProperties.BAR_SPACING);

            animatedStep = Math.abs(selectedIndex - i); // get distance to the selected bar

            if (animatedStep <= animatedRange){ // current bar should be shifted
                spacing = (float) (defaultSpacing * Math.pow(spacingFactor - spacingSizeAdj * defaultSpacing, animatedRange - animatedStep));
                xStep = (float) (xDisplacement * Math.pow(xStepFactor, animatedStep));
            }
            else{
                spacing = defaultSpacing;
                xStep = 0;// no displacement
            }

            if (animatedStep > animatedRange)
                thickness = (float) (defaultThickness * Math.pow(adaptedThicknessFactor, animatedStep - animatedRange));

            else {
                thickness = defaultThickness;
            }

            barThickness.setValue(thickness);
            barSpacing.setValue(spacing);

            y += spacing; // add leading spacing
            float yEnd = y + thickness;
            y = yEnd + spacing;

            x = b.getValue() + xStep;
            if (x > barsWidth) barsWidth = x;
        }
        float firstSpaceing = barsView.getBar(0).getProperty(BarsProperties.BAR_SPACING).getValue();
        float lastSpasing = barsView.getBar(barsView.size() - 1).getProperty(BarsProperties.BAR_SPACING).getValue();
        barsHeight = (y - firstSpaceing - lastSpasing);
        barsWidth /= 0.7;
    }

    @Override
    protected void recalculateAnimatedBarsScale() {
        float drawingHeight = getDrawingHeight(); // width of the drawing area

        heightScaleFactor = drawingHeight / barsHeight;
        heightScaleFactor = (heightScaleFactor > 1 ? 1 : heightScaleFactor);
    }

    @Override
    protected void recalculateAnimatedBarsPositions() {

        final float left = getBarsBorder(Side.LEFT);
        final float right = getBarsBorder(Side.RIGHT);
        final float bottom = getBarsBorder(Side.BOTTOM);
        final float top = getBarsBorder(Side.TOP);

        startY = (getScreenHeight() - getBarsHeight() * heightScaleFactor) / 2;
        startY = (startY < top ? top : startY);

        final int selectedIndex = barsView.getSelectedIndex();
        int animatedStep;

        float y = startY;


        float x;
        float xStep;
        float thickness;
        float spacing;

        for (int i = 0; i < barsView.getBarsData().size(); ++i) {
            Bar b = barsView.getBarsData().getBar(i);
            thickness = b.getProperty(BarsProperties.BAR_THICKNESS).getValue() * heightScaleFactor;
            spacing =  b.getProperty(BarsProperties.BAR_SPACING).getValue() * heightScaleFactor;

            animatedStep = Math.abs(selectedIndex - i); // get distance to the selected bar

            if (animatedStep <= animatedRange){ // current bar should be shifted
                xStep = (float) (xDisplacement * Math.pow(xStepFactor, animatedStep));
            }
            else{
                xStep = 0;// no displacement
            }
            y += spacing; // add leading spacing
            float yEnd = y + thickness;
            yEnd = (yEnd >= bottom ? bottom : yEnd);

            switch (barsView.getBarsAlignment()) {
                default:
                case LEFT:
                    x = b.getValue() * widthScaleFactor;
                    b.getDrawingRect().set(left + xStep, y, x + xStep, yEnd);
                    break;
                case RIGHT:
                    x = right - b.getValue() * widthScaleFactor;
                    b.getDrawingRect().set(x - xStep, y, right - xStep, yEnd);
                    break;
            }
            y = yEnd + spacing;
        }
    }

    @Override
    protected void recalculateAnimatedBarsLabels() {
        super.recalculateBarsLabels();
    }

}
