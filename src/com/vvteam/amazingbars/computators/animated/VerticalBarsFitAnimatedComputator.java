package com.vvteam.amazingbars.computators.animated;

import com.vvteam.amazingbars.BarsView;
import com.vvteam.amazingbars.computators.Side;
import com.vvteam.amazingbars.computators.normal.VerticalBarsComputator;
import com.vvteam.amazingbars.data.Bar;
import com.vvteam.amazingbars.properties.BarsProperties;
import com.vvteam.amazingbars.properties.BarsProperty;

/** Computator which compute bars' positions with extra space to fit screen. */
public class VerticalBarsFitAnimatedComputator extends VerticalBarsComputator{

    private float yDisplacement = 25;      // base displacement of selected bar.
    private float animatedRange = 7;       // amount of animated bars from both sides of selected bar.
    private float yStepFactor = 0.5f;      // step multiplier. Lowering away from selected bar.
    private float spacingFactor = 1.2f;    // spacing multiplier. Lowering away from selected bar.
    private float spacingSizeAdj = 0.008f; // adjust spacing multiplier based on how great is base spacing (to avoid overplaping when default spacing was set too big).

    private float animationExtaSpace;
    private float actualWidth;

    public VerticalBarsFitAnimatedComputator(BarsView barsView) {
        super(barsView);
    }


    private void calculateBarsSize(int selectedIndex){
        barsWidth = barsHeight = 0;
        int animatedStep;

        float x = 0;
        float y;
        float yStep;

        final float defaultSpacing = barsView.getProperty(BarsProperties.BAR_SPACING).getValue();
        final float defaultThickness = barsView.getProperty(BarsProperties.BAR_THICKNESS).getValue();
        float spacing;

        for (int i = 0; i < barsView.size(); ++i) {
            Bar b = barsView.getBar(i);
            BarsProperty barThickness = b.getProperty(BarsProperties.BAR_THICKNESS);
            BarsProperty barSpacing = b.getProperty(BarsProperties.BAR_SPACING);

            animatedStep = Math.abs(selectedIndex - i); // get distance to the selected bar

            if (animatedStep <= animatedRange){ // current bar should be shifted
                spacing = (float) (defaultSpacing * Math.pow(spacingFactor - spacingSizeAdj * defaultSpacing, animatedRange - animatedStep));
                yStep = (float) (yDisplacement * Math.pow(yStepFactor, animatedStep));
            }
            else{
                spacing = defaultSpacing;
                yStep = 0;// no displacement
            }

            barThickness.setValue(defaultThickness);
            barSpacing.setValue(spacing);

            x += spacing; // add leading spacing
            float xEnd = x + defaultThickness;
            x = xEnd + spacing;

            y = b.getValue() + yStep;
            if (y > barsHeight) barsHeight = y;
        }
        float firstSpaceing = 0;
        float lastSpasing = 0;
        if (barsView.size() > 0) {
            firstSpaceing = barsView.getBar(0).getProperty(BarsProperties.BAR_SPACING).getValue();
            lastSpasing = barsView.getBar(barsView.size() - 1).getProperty(BarsProperties.BAR_SPACING).getValue();
        }
        barsWidth = (x - firstSpaceing - lastSpasing);
        barsHeight /= 0.7;
    }

    @Override
    protected void recalculateBarsSize() {
        super.recalculateBarsSize();
        actualWidth = barsWidth;
        calculateBarsSize(barsView.size() / 2);
        animationExtaSpace = barsWidth - actualWidth;
    }

    @Override
    protected void recalculateBarsPositions() {
        float x = 0; // determine starting x-pos depending on alignment
        float y = getBarsBorder(Side.BOTTOM);
        switch (barsView.getBarsAlignment()) {
            case LEFT:
                startX =  getBarsBorder(Side.LEFT);// + animationExtaSpace * widthScaleFactor / 2;
                break;
            case RIGHT:
                startX = getBarsBorder(Side.RIGHT) - actualWidth * widthScaleFactor; // - animationExtaSpace * widthScaleFactor / 2;
                break;
            case CENTER:
                startX = (getScreenWidth() - getBarsWidth() * widthScaleFactor) / 2 + animationExtaSpace * widthScaleFactor/2;
                startX = (startX < getBarsBorder(Side.LEFT) ? getBarsBorder(Side.LEFT) : startX);
                break;
        }
        x = startX;
        for (Bar b : barsView.getBarsData()) {
            float spacing = barsView.getProperty(BarsProperties.BAR_SPACING).getValue() * widthScaleFactor;
            x += spacing;
            float xEnd = x + b.getProperty(BarsProperties.BAR_THICKNESS).getValue() * widthScaleFactor;
            xEnd = (xEnd >= getBarsBorder(Side.RIGHT) ? getBarsBorder(Side.RIGHT) : xEnd);
            b.getDrawingRect().set(x, y - b.getValue() * heightScaleFactor, xEnd, y);
            x = xEnd + spacing;
        }
    }

    @Override
    protected void recalculateAnimatedBarsSize() {
        calculateBarsSize(barsView.getSelectedIndex());
    }

    @Override
    protected void recalculateAnimatedBarsScale() {

    }

    @Override
    protected void recalculateAnimatedBarsPositions() {
        switch (barsView.getBarsAlignment()) {
            case LEFT:
                startX =  getBarsBorder(Side.LEFT);
                break;
            case RIGHT:
                startX = getBarsBorder(Side.RIGHT) - getBarsWidth() * widthScaleFactor;
                break;
            case CENTER:
                startX = (getScreenWidth() - getBarsWidth() * widthScaleFactor) / 2;
                startX = (startX < getBarsBorder(Side.LEFT) ? getBarsBorder(Side.LEFT) : startX);
                break;
        }
        startY = getBarsBorder(Side.BOTTOM);

        final int selectedIndex = barsView.getSelectedIndex();
        int animatedStep;

        float x = startX;

        float y;
        float yStep;
        float spacing;
        final float defaultThickness = barsView.getProperty(BarsProperties.BAR_THICKNESS).getValue() * widthScaleFactor;

        for (int i = 0; i < barsView.getBarsData().size(); ++i) {
            Bar b = barsView.getBarsData().getBar(i);
            spacing =  b.getProperty(BarsProperties.BAR_SPACING).getValue() * widthScaleFactor;

            animatedStep = Math.abs(selectedIndex - i); // get distance to the selected bar

            if (animatedStep <= animatedRange){ // current bar should be shifted
                yStep = (float) (yDisplacement * Math.pow(yStepFactor, animatedStep));
            }
            else{
                yStep = 0;// no displacement
            }
            x += spacing; // add leading spacing
            y = startY - b.getValue() * heightScaleFactor - yStep;
            float xEnd = x + defaultThickness;
            xEnd = (xEnd >= getBarsBorder(Side.RIGHT) ? getBarsBorder(Side.RIGHT) : xEnd);
            b.getDrawingRect().set(x, y, xEnd, startY - yStep);
            x = xEnd + spacing;
        }
    }

    @Override
    protected void recalculateAnimatedBarsLabels() {
        super.recalculateBarsLabels();
    }
}
