package com.vvteam.amazingbars.computators.normal;

import android.graphics.Rect;
import android.graphics.RectF;
import com.vvteam.amazingbars.BarsView;
import com.vvteam.amazingbars.computators.Side;
import com.vvteam.amazingbars.data.Bar;
import com.vvteam.amazingbars.properties.BarsProperties;

public abstract class VerticalBarsComputator extends BarsComputator {

    public VerticalBarsComputator(BarsView barsView) {
        super(barsView);
    }

    @Override
    public int hitTest(float x, float y) {
        float minDX = Float.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < barsView.getBarsData().size(); ++i) {
            RectF rect = barsView.getBarsData().getBar(i).getDrawingRect();
            float closestX = Math.min(Math.abs(rect.left - x), Math.abs(rect.right - x)); // closest side to touch pos

            if (closestX < minDX){
                minDX = closestX;
                index = i;
            }
        }
        return index;
    }


    @Override
    protected void recalculateBarsSize(){
        int width = 0;
        float height = 0;
        float spacing = barsView.getProperty(BarsProperties.BAR_SPACING).getValue();
        float thickness = barsView.getProperty(BarsProperties.BAR_THICKNESS).getValue();
        for (Bar b : barsView.getBarsData()) {
            b.getProperty(BarsProperties.BAR_THICKNESS).setValue(thickness);
            b.getProperty(BarsProperties.BAR_SPACING).setValue(spacing);
            width += spacing + thickness + spacing;
            if (b.getValue() > height) height = b.getValue();
        }
        if (barsView.size() > 0)
        width -= spacing; // subtract one extra leading spacing
        width -= spacing; // subtract one extra trailing spacing

        barsWidth = width;
        barsHeight = (float) (height / 0.7);
    }

    @Override
    protected void recalculateBarsScale(){
        float drawingWidth = getDrawingWidth(); // width of the drawing area
        float drawingHeight = getDrawingHeight(); // height of the drawing area

        widthScaleFactor = drawingWidth / barsWidth;
        widthScaleFactor = (widthScaleFactor > 1 ? 1 : widthScaleFactor);

        heightScaleFactor = drawingHeight / barsHeight;
        heightScaleFactor = (heightScaleFactor > 1 ? 1 : heightScaleFactor);
        heightScaleFactor *= widthScaleFactor; // add width scale factor to save aspect
    }

    @Override
    protected void recalculateBarsPositions() {
        float x = 0; // determine starting x-pos depending on alignment
        float y = getBarsBorder(Side.BOTTOM);
        switch (barsView.getBarsAlignment()) {
            case LEFT:
                x =  getBarsBorder(Side.LEFT);
                break;
            case RIGHT:
                x = getBarsBorder(Side.RIGHT) - getBarsWidth() * widthScaleFactor;
                break;
            case CENTER:
                x = (getScreenWidth() - getBarsWidth() * widthScaleFactor) / 2;
                x = (x < getBarsBorder(Side.LEFT) ? getBarsBorder(Side.LEFT) : x);
                break;
        }
        for (Bar b : barsView.getBarsData()) {
            float spacing = barsView.getProperty(BarsProperties.BAR_SPACING).getValue() * widthScaleFactor;
            x += spacing;
            float xEnd = x + b.getProperty(BarsProperties.BAR_THICKNESS).getValue() * widthScaleFactor;
            xEnd = (xEnd >= getBarsBorder(Side.RIGHT) ? getBarsBorder(Side.RIGHT) : xEnd);
            b.getDrawingRect().set(x, y - b.getValue() * heightScaleFactor, xEnd, y);
            x = xEnd + spacing;
        }
    }

    @Override
    protected void recalculateBarsLabels() {
        Rect labelBounds;
        RectF drawingRect;
        float lX,lY;
        for (Bar b : barsView.getBarsData()) {
            if (!barsView.isShowAllValues()) {
                b = barsView.getSelectedBar();   // instantly go to the selected bar and calculate its' position
                if (b == null) break; // if no selection - break;
            }
            drawingRect = b.getDrawingRect();
            labelBounds = getTextBounds(b.getValueString(), b.getProperty(BarsProperties.BAR_LABEL_SIZE).getValue());
            lX = drawingRect.left + (drawingRect.width() - labelBounds.width())/2;
            lY = drawingRect.top - labelBounds.height();
            b.getLabelValue().set(lX, lY - labelBounds.height(), lX + labelBounds.width(), lY);
            if (!barsView.isShowAllValues()) {
                break; // break loop and do not calculate others
            }
        }
    }
}
