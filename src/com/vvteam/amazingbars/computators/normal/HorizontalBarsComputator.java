package com.vvteam.amazingbars.computators.normal;

import android.graphics.Rect;
import android.graphics.RectF;
import com.vvteam.amazingbars.BarsView;
import com.vvteam.amazingbars.computators.Side;
import com.vvteam.amazingbars.data.Bar;
import com.vvteam.amazingbars.properties.BarsProperties;

public abstract class HorizontalBarsComputator extends BarsComputator{

    public HorizontalBarsComputator(BarsView barsView) {
        super(barsView);
    }

    @Override
    public int hitTest(float x, float y) {
        float minDY = Float.MAX_VALUE;
        int index = -1;
        for (int i = 0; i < barsView.getBarsData().size(); ++i) {
            RectF rect = barsView.getBarsData().getBar(i).getDrawingRect();
            float closestY = Math.min(Math.abs(rect.top - y), Math.abs(rect.bottom - y)); // closest side to touch pos

            if (closestY < minDY){
                minDY = closestY;
                index = i;
            }
        }
        return index;
    }


    @Override
    protected void recalculateBarsSize(){
        float width = 0;
        float height = 0;
        float spacing = barsView.getProperty(BarsProperties.BAR_SPACING).getValue();
        float thickness = barsView.getProperty(BarsProperties.BAR_THICKNESS).getValue();
        for (Bar b : barsView.getBarsData()) {
            b.getProperty(BarsProperties.BAR_THICKNESS).setValue(thickness);
            b.getProperty(BarsProperties.BAR_SPACING).setValue(spacing);
            height += spacing + thickness + spacing;
            if (b.getValue() > width) width = b.getValue();
        }
        height -= spacing; // subtract one extra leading spacing
        height -= spacing; // subtract one extra trailing spacing

        barsWidth = (float) (width / 0.7);
        barsHeight = height;
    }

    @Override
    protected void recalculateBarsScale(){
        float drawingWidth = getDrawingWidth(); // width of the drawing area
        float drawingHeight = getDrawingHeight(); // height of the drawing area

        heightScaleFactor = drawingHeight / barsHeight;
        heightScaleFactor = (heightScaleFactor > 1 ? 1 : heightScaleFactor);

        widthScaleFactor = drawingWidth / barsWidth;
        widthScaleFactor = (widthScaleFactor > 1 ? 1 : widthScaleFactor);
        widthScaleFactor *= heightScaleFactor; // add height scale factor to save aspect

    }

    @Override
    protected void recalculateBarsPositions() {
        float x = 0; // determine starting x-pos depending on alignment
        float y = 0;

        y = (getScreenHeight() - getBarsHeight() * heightScaleFactor) / 2;
        y = (y < getBarsBorder(Side.TOP) ? getBarsBorder(Side.TOP) : y);

        float left = getBarsBorder(Side.LEFT);
        float right = getBarsBorder(Side.RIGHT);

        for (Bar b : barsView.getBarsData()) {
            float spacing = barsView.getProperty(BarsProperties.BAR_SPACING).getValue() * heightScaleFactor;
            y += spacing;
            float yEnd = y + b.getProperty(BarsProperties.BAR_THICKNESS).getValue() * heightScaleFactor;
            yEnd = (yEnd >= getBarsBorder(Side.BOTTOM) ? getBarsBorder(Side.BOTTOM) : yEnd);

            switch (barsView.getBarsAlignment()) {
                default:
                case LEFT:
                     x = left;
                    b.getDrawingRect().set(x, y, x + b.getValue() * widthScaleFactor, yEnd);
                    break;
                case RIGHT:
                    x = right;
                    b.getDrawingRect().set(x - b.getValue() * widthScaleFactor, y, x, yEnd);
                    break;
            }


            y = yEnd + spacing;
        }
    }

    @Override
    protected void recalculateBarsLabels() {
        Rect labelBounds;
        RectF drawingRect;
        float lX,lY;
        for (Bar b : barsView.getBarsData()) {
            if (!barsView.isShowAllValues()) {
                b = barsView.getSelectedBar();   // instantly go to the selected bar and calculate its' position
                if (b == null) break; // if no selection - break;
            }
            drawingRect = b.getDrawingRect();
            labelBounds = getTextBounds(b.getValueString(), b.getProperty(BarsProperties.BAR_LABEL_SIZE).getValue());
            switch (barsView.getBarsAlignment()) {
                default:
                case LEFT:
                    lX = drawingRect.right + 3;
                    break;
                case RIGHT:
                    lX = drawingRect.left - labelBounds.width() - 3;
                    break;
            }

            lY = drawingRect.top + labelBounds.height();
            b.getLabelValue().set(lX, lY - labelBounds.height(), lX + labelBounds.width(), lY);
            if (!barsView.isShowAllValues()) {
                break; // break loop and do not calculate others
            }
        }
    }
}
