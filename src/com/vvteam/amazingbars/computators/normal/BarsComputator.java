package com.vvteam.amazingbars.computators.normal;


import android.graphics.Paint;
import android.graphics.Rect;
import com.vvteam.amazingbars.BarsView;
import com.vvteam.amazingbars.computators.Side;
import com.vvteam.amazingbars.properties.BarsProperties;

public abstract class BarsComputator {

    protected BarsView barsView;

    protected float screenWidth; // actual width of the view
    protected float screenHeight; // actual height of the view

    protected float barsWidth; // calculated width of the bars drawing area
    protected float barsHeight; // calculated height of the bars drawing area

    protected float startX;
    protected float startY;

    protected float widthScaleFactor; // used to match screen width
    protected float heightScaleFactor; // used to match screen height

    private Paint textMeasurer = new Paint();  // used for measure text
    private Rect lastMeasuredTextRect = new Rect(); // used for store results of measuring.

    protected BarsComputator(BarsView barsView) {
        this.barsView = barsView;
        widthScaleFactor = 1;
    }

    public void setScreenSize(float screenWidth, float screenHeight){
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;
        recalculateMetrics();
    }

    /** Returns offset of bars drawing area.*/
    public float getOffset(){
        return barsView.getProperty(BarsProperties.VIEW_BORDER).getValue() + barsView.getProperty(BarsProperties.VIEW_PADDING).getValue();
    }

    /** Returns bounds of bars drawing area. */
    public float getBarsBorder(Side side) {
        switch (side) {
            case LEFT:
            case TOP:  return getBarsOffset(side);
            case RIGHT: return screenWidth - getBarsOffset(Side.RIGHT);
            case BOTTOM: return screenHeight - getBarsOffset(Side.BOTTOM);
            default: return 0;
        }
    }

    /** Returns offsets of bars drawing area. */
    public float getBarsOffset(Side side){
        float offset = getOffset();
        switch (side) {
            case LEFT: return offset;
            case TOP:  return offset;
            case RIGHT: return offset;
            case BOTTOM: return offset;
            default: return 0;
        }
    }

    public abstract int hitTest(float x, float y);

    protected abstract void recalculateBarsSize();
    protected abstract void recalculateBarsScale();
    protected abstract void recalculateBarsPositions();
    protected abstract void recalculateBarsLabels();

    protected abstract void recalculateAnimatedBarsSize();
    protected abstract void recalculateAnimatedBarsScale();
    protected abstract void recalculateAnimatedBarsPositions();
    protected abstract void recalculateAnimatedBarsLabels();

    /** Recalculates all parameters. */
    public final void recalculateMetrics(){
        if (barsView.getTouchHandler().isTouched()){
            recalculateAnimatedBarsSize();
            recalculateAnimatedBarsScale();
            recalculateAnimatedBarsPositions();
            recalculateAnimatedBarsLabels();
        }
        else {
            recalculateBarsSize();
            recalculateBarsScale();
            recalculateBarsPositions();
            recalculateBarsLabels();
        }
    }

    public final void recalculateLabels(){
        if (barsView.getTouchHandler().isTouched()){
            recalculateAnimatedBarsLabels();
        }
        else {
            recalculateBarsLabels();
        }
    }



    public final Rect getTextBounds(String text, float textSize){
        textMeasurer.setTextSize(textSize);
        textMeasurer.getTextBounds(text, 0, text.length(), lastMeasuredTextRect);
        return lastMeasuredTextRect;
    }

    public final float getScreenWidth() {
        return screenWidth;
    }

    public final float getScreenHeight() {
        return screenHeight;
    }

    public final float getBarsWidth() {
        return barsWidth;
    }

    public final float getBarsHeight() {
        return barsHeight;
    }

    public final float getDrawingWidth() {
        return getBarsBorder(Side.RIGHT) - getBarsBorder(Side.LEFT);
    }

    public final float getDrawingHeight() {
        return getBarsBorder(Side.BOTTOM) - getBarsBorder(Side.TOP);
    }

    public final float getDesiredWidth() {
        return barsWidth + getBarsOffset(Side.LEFT) + getBarsOffset(Side.RIGHT);
    }

    public final float getDesiredHeight(){
        return barsHeight + getBarsOffset(Side.BOTTOM) + getBarsOffset(Side.TOP);
    }

    public final float getWidthScaleFactor() {
        return widthScaleFactor;
    }

    public float getHeightScaleFactor() {
        return heightScaleFactor;
    }

    public float getStartX() {
        return startX;
    }

    public float getStartY() {
        return startY;
    }
}
