package com.vvteam.amazingbars.renderers;

import android.graphics.Paint;
import android.graphics.RectF;
import com.vvteam.amazingbars.BarsView;
import com.vvteam.amazingbars.data.Bar;
import com.vvteam.amazingbars.data.BarColors;
import com.vvteam.amazingbars.properties.BarsProperties;

public class VerticalBarsRenderer extends BarsRenderer {
    public VerticalBarsRenderer(BarsView barsView) {
        super(barsView);
    }

    @Override
    protected void drawBars(){
        for (Bar b : barsView.getBarsData()) {
            barPaint.setColor(b.isSelected() ? b.getColor(BarColors.COLOR_BAR_SELECTED) : b.getColor(BarColors.COLOR_BAR_NORMAL));
            barPaint.setStyle(Paint.Style.FILL);
            capturedCanvas.drawRect(b.getDrawingRect(), barPaint);

            barPaint.setStyle(Paint.Style.STROKE);
            barPaint.setStrokeWidth(b.getProperty(BarsProperties.BAR_BORDER_THICKNESS).getValue());
            barPaint.setColor(b.isSelected() ? b.getColor(BarColors.COLOR_BORDER_SELECTED) : b.getColor(BarColors.COLOR_BORDER_NORMAL));
            capturedCanvas.drawRect(b.getDrawingRect(), barPaint);
        }
    }

    @Override
    protected void drawLabels(){
        Bar selected = null;
        auxPaint.setStrokeWidth(1f);
        for (Bar b : barsView.getBarsData()) {
            if (b.isSelected()) {
                selected = b;
                continue;
            }
            if (barsView.isShowAllValues())
                drawBarLabel(b);
        }

        if (selected != null){
            drawBarLabel(selected);
        }
    }

    @Override
    protected void drawBarLabel(Bar bar){
        RectF valuePos = bar.getLabelValue();
        auxPaint.setStyle(Paint.Style.FILL);
        auxPaint.setColor(bar.isSelected() ? bar.getColor(BarColors.COLOR_LABEL_BG_SELECTED) : bar.getColor(BarColors.COLOR_LABEL_BG_NORMAL));
        auxPaint.setAlpha((int) (255f * bar.getProperty(BarsProperties.BAR_LABEL_ALPHA).getValue()));
        capturedCanvas.drawRect(valuePos.left - 3, valuePos.top - 3, valuePos.right + 3, valuePos.bottom + 3, auxPaint);

        auxPaint.setStyle(Paint.Style.STROKE);
        auxPaint.setColor(bar.isSelected() ? bar.getColor(BarColors.COLOR_LABEL_SELECTED) : bar.getColor(BarColors.COLOR_LABEL_NORMAL));
        auxPaint.setAlpha(255);
        auxPaint.setTextSize(bar.getProperty(BarsProperties.BAR_LABEL_SIZE).getValue());
        capturedCanvas.drawText(bar.getValueString(), valuePos.left, valuePos.bottom, auxPaint);
    }
}
