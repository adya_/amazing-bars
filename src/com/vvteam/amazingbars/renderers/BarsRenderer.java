package com.vvteam.amazingbars.renderers;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import com.vvteam.amazingbars.BarsView;
import com.vvteam.amazingbars.computators.normal.BarsComputator;
import com.vvteam.amazingbars.data.Bar;
import com.vvteam.amazingbars.properties.BarsProperties;

public abstract class BarsRenderer{

    protected BarsView barsView;

    protected Paint barPaint; // used for drawing bars.
    protected Paint auxPaint; // used for drawing secondary stuff like background.

    protected Canvas capturedCanvas;

    public BarsRenderer(BarsView barsView){
        this.barsView = barsView;
        initPaints();
    }

    protected void initPaints(){
        barPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        barPaint.setStyle(Paint.Style.FILL);

        auxPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        auxPaint.setStyle(Paint.Style.FILL);
        auxPaint.setColor(Color.WHITE);
    }

    public final void draw(Canvas canvas){
        capturedCanvas = canvas;
        drawBackground();
        drawBars();
        drawLabels();
        capturedCanvas = null;
    }

    protected void drawBackground() {
        BarsComputator comp = barsView.getComputator();
        auxPaint.setStyle(Paint.Style.FILL);
        auxPaint.setColor(barsView.getBackgroundColor());
        capturedCanvas.drawRect(0, 0, comp.getScreenWidth(), comp.getScreenHeight(), auxPaint);

        auxPaint.setStyle(Paint.Style.STROKE);
        auxPaint.setStrokeWidth(barsView.getProperty(BarsProperties.VIEW_BORDER).getValue());
        auxPaint.setColor(barsView.getBorderColor());
        capturedCanvas.drawRect(0, 0, comp.getScreenWidth(), comp.getScreenHeight(), auxPaint);
    }

    protected abstract void drawBars();

    protected abstract void drawLabels();

    protected abstract void drawBarLabel(Bar bar);
}
