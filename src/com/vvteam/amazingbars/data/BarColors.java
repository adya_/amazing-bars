package com.vvteam.amazingbars.data;

import android.graphics.Color;

public class BarColors {
    public static final int COLOR_BAR_NORMAL = 0;
    public static final int COLOR_BORDER_NORMAL = 1;
    public static final int COLOR_LABEL_NORMAL = 2;
    public static final int COLOR_LABEL_BG_NORMAL = 3;
    public static final int COLOR_BAR_SELECTED = 4;
    public static final int COLOR_BORDER_SELECTED = 5;
    public static final int COLOR_LABEL_SELECTED = 6;
    public static final int COLOR_LABEL_BG_SELECTED = 7;
    public static final int COLORS_AMOUNT = 8;

    private int[] colors;

    public BarColors(int normalBarColor) {
        this(normalBarColor, normalBarColor);
    }

    public BarColors(int normalBarColor, int selectedBarColor) {
        this(normalBarColor, normalBarColor, selectedBarColor, selectedBarColor);
    }

    public BarColors(int normalBarColor, int normalBorderColor, int selectedBarColor, int selectedBorderColor){
        this(normalBarColor, normalBorderColor, normalBarColor, Color.TRANSPARENT, selectedBarColor, selectedBorderColor, selectedBarColor, Color.TRANSPARENT);
    }

    public BarColors(int normalBarColor, int normalBorderColor, int normalLabelColor, int normalLabelBGColor, int selectedBarColor, int selectedBorderColor, int selectedLabelColor, int selecteLabelBGColor){
        colors = new int[]{normalBarColor, normalBorderColor, normalLabelColor, normalLabelBGColor, selectedBarColor, selectedBorderColor, selectedLabelColor, selecteLabelBGColor};
    }

    public int getColor(int colorIndex){
        if (colorIndex < 0 || colorIndex >= COLORS_AMOUNT) return 0;
        return colors[colorIndex];
    }

    public  void setColor(int colorIndex, int colorValue){
        if (colorIndex < 0 || colorIndex >= COLORS_AMOUNT) return;
        colors[colorIndex] = colorValue;
    }

    public void set(BarColors pallete) {
        if (pallete == null) return;
        for (int i = 0; i < colors.length; i++) {
            colors[i] = pallete.colors[i];
        }
    }
}
