package com.vvteam.amazingbars.data;

import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.Typeface;
import com.vvteam.amazingbars.properties.BarsProperties;
import com.vvteam.amazingbars.properties.BarsProperty;
import com.vvteam.amazingbars.properties.NoSuchPropertyException;

import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public class Bar implements Comparable<Bar> {

    private long id;
    private static AtomicLong idCounter = new AtomicLong();
    private static long createID(){
        return idCounter.getAndIncrement();
    }

    private float value = 0;

    private BarColors colors;


    private boolean isSelected;
    private RectF drawingRect; // used for position pre-calculations
    private RectF labelValue; // value label position
    private Typeface labelTypeface;

    private HashMap<String, BarsProperty> barProperties;

    public Bar(float value) {
        this.id = Bar.createID();
        this.value = value;
        this.colors = new BarColors(Color.WHITE, Color.LTGRAY, Color.GREEN, Color.BLUE);
        setColor(BarColors.COLOR_LABEL_BG_NORMAL, Color.BLACK);
        setColor(BarColors.COLOR_LABEL_BG_SELECTED, Color.BLACK);
        this.isSelected = false;
        this.drawingRect = new RectF();
        this.labelValue = new RectF();
        labelTypeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL);

        barProperties = new HashMap<>();
        resetProperties();

    }

    public void resetProperties(){
        barProperties.clear();
        barProperties.put(BarsProperties.BAR_LABEL_SIZE, new BarsProperty(8, 18, 12, BarsProperties.BAR_LABEL_SIZE));
        barProperties.put(BarsProperties.BAR_THICKNESS, new BarsProperty(2, 40, 20, BarsProperties.BAR_THICKNESS));
        barProperties.put(BarsProperties.BAR_SPACING, new BarsProperty(2, 15, 2, BarsProperties.BAR_SPACING));
        barProperties.put(BarsProperties.BAR_BORDER_THICKNESS, new BarsProperty(0, 5, 2, BarsProperties.BAR_BORDER_THICKNESS));
        barProperties.put(BarsProperties.BAR_LABEL_ALPHA, new BarsProperty(0, 1, 0.6f, BarsProperties.BAR_LABEL_ALPHA));
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getValueString(){
        return getValueString("%.2f");
    }

    public String getValueString(String format){
        return String.format(format, value);
    }


    public int getColor(int colorIndex) {
        return colors.getColor(colorIndex);
    }

    public void setColor(int colorIndex, int colorValue) {
        this.colors.setColor(colorIndex, colorValue);
    }

    public BarColors getColorsPallete(){
        return colors;
    }

    public void setColorsPallete(BarColors pallete){
        this.colors.set(pallete);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    /** Enumerate all properties of the bars. */
    public Collection<BarsProperty> getProperties(){
        return barProperties.values();
    }

    public BarsProperty getProperty(String name){
        BarsProperty prop = barProperties.get(name);
        if (prop == null){
            throw new NoSuchPropertyException("Property '" + name + "' doesn't exist.");
        }
        return prop;
    }

    public void setProperty(BarsProperty property) {
        barProperties.put(property.getName(), new BarsProperty(property));
    }

    public RectF getDrawingRect() {
        return drawingRect;
    }

    public RectF getLabelValue() {
        return labelValue;
    }

    public Typeface getLabelTypeface() {
        return labelTypeface;
    }

    public void setLabelTypeface(Typeface labelTypeface) {
        this.labelTypeface = labelTypeface;
    }


    @Override
    public int compareTo(Bar bar) {
        if (bar == null) return 1; // this will be greater than null-bar.
        return (int) Math.signum(value - bar.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bar bar = (Bar) o;
        return id == bar.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return String.format("%.2f", value);
    }
}
