package com.vvteam.amazingbars.data;

import java.util.Collection;

public interface BarsCollection {
    public boolean addBar(Bar bar);
    public boolean addBar(int index, Bar bar);
    public boolean addBars(Collection<Bar> bars);

    public boolean removeBar(Bar bar);
    public boolean removeBar(int index);
    public void removeAll();

    public boolean contains(Bar bar);

    public Bar getBar(int index);
    public int indexOf(Bar bar);
    public int size();
}
