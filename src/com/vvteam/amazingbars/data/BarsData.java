package com.vvteam.amazingbars.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class BarsData implements Iterable<Bar>, BarsCollection {
    private ArrayList<Bar> bars;

    public BarsData() {
        bars = new ArrayList<>();
    }

    @Override
    public boolean addBar(Bar bar) {
        return addBar(bars.size(), bar);
    }

    @Override
    public boolean addBar(int index, Bar bar) {
        if (((index >= 0) && (index <= bars.size())) && ((bar != null) && !bars.contains(bar))) {
            bars.add(index, bar);
            return true;
        }
        return false;
    }

    @Override
    public boolean addBars(Collection<Bar> bars) {
        return bars != null && bars.addAll(bars);
    }

    @Override
    public boolean removeBar(Bar bar) {
        return (bar != null) && bars.remove(bar);

    }

    @Override
    public boolean removeBar(int index) {
        return (index >= 0 && index < bars.size()) && (bars.remove(index) != null);
    }

    @Override
    public void removeAll() {
        bars.clear();
    }

    @Override
    public boolean contains(Bar bar) {
        return bars.contains(bar);
    }

    @Override
    public int size() {
        return bars.size();
    }

    @Override
    public Bar getBar(int index){
        if (index < 0 || index >= bars.size()) return null;
        return bars.get(index);
    }

    @Override
    public int indexOf(Bar bar) {
        if (bar == null) return -1;
        return bars.indexOf(bar);
    }

    @Override
    public Iterator<Bar> iterator() {
        return bars.iterator();
    }
}
