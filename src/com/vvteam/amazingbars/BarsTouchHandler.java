package com.vvteam.amazingbars;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;

public class BarsTouchHandler implements View.OnTouchListener{

    protected BarsView barsView;
    protected int selectedBarIndex;
    protected int lastSelectedIndex = -1;

    protected boolean isTouched = false;

    public boolean isTouched() {
        return isTouched;
    }

    public BarsTouchHandler(BarsView barsView){
        this.barsView = barsView;
    }

    private void lockTouch(){
        ViewParent root = barsView.getParent();
        if (root == null) return;
        do {
            root.requestDisallowInterceptTouchEvent(true);
        }while ((root = root.getParent()) != null);
    }

    @Override
    public final boolean onTouch(View view, MotionEvent motionEvent) {
        lockTouch();
        float x = motionEvent.getX();
        float y = motionEvent.getY();
        selectedBarIndex = barsView.getComputator().hitTest(x, y);
        switch (motionEvent.getActionMasked()) {
            case MotionEvent.ACTION_DOWN: {
                // start position calculations, and animate bars
                isTouched = true;
                if (selectedBarIndex >= 0) {
                    barsView.selectBar(selectedBarIndex);
                    barsView.invalidate();
                }
                return true;
            }
            case MotionEvent.ACTION_MOVE: {
                // calculate positions and animate bars
                if (selectedBarIndex >= 0 && selectedBarIndex != lastSelectedIndex) {
                    barsView.selectBar(selectedBarIndex);
                    barsView.getComputator().recalculateMetrics();
                    barsView.invalidate();
                    lastSelectedIndex = selectedBarIndex;
                }
                return true;
            }
            case MotionEvent.ACTION_UP: {
                // end calculations, normalize bars, set selected bar
                isTouched = false;
                if (barsView.isRememberSelection())  {
                    barsView.selectBar(selectedBarIndex);
                    lastSelectedIndex = selectedBarIndex;
                }
                else {
                    barsView.deselectBar();
                    selectedBarIndex = lastSelectedIndex = -1;
                }
                barsView.getComputator().recalculateMetrics();
                return true;
            }
            case MotionEvent.ACTION_CANCEL:{
                // end calculations, normalize bars
                isTouched = false;
                barsView.deselectBar();
                barsView.getComputator().recalculateMetrics();
                selectedBarIndex = lastSelectedIndex = -1;
                return true;
            }
            default: return false;
        }
    }


}
